use super::Value;

pub struct JsUndefined(pub(crate) Value);
